import PropTypes from 'prop-types';



function Button (props){
    const { color, backgroundColor,size, title, click, id} = props
    Button.id = id;
    
    const styles = {
        color,
        backgroundColor, 
        fontSize: size        
      }

    return(
        <button onClick={() => {click()}} style={styles} >
        {title}
        </button>
    );

}
Button.propTypes = {
    color: PropTypes.string, 
    backgroundColor: PropTypes.string, 
    size: PropTypes.number,
    title: PropTypes.string.isRequired, 
    test:PropTypes.string
    
  }
  Button.defaultProps = {
    color: 'black',
    backgroundColor: 'white',
    size: 14,
    title:'test',
  }



export default Button;