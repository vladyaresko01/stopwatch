import Button from "../Button/Button"
import Dial from "../Dial/Dial"
import React, { useState, useEffect} from 'react';

import { timer,fromEvent, Observable} from 'rxjs';
import { repeatWhen, switchMap, takeUntil} from 'rxjs/operators';



function Stopwatch(){
    const [time,setTime] = useState(new Date(0));
    // time.setHours(0);
    const [isStarted,setIsStarted] = useState(false)
    const [lastC, setLastC] = useState();
    const start$ = fromEvent(document.getElementById('start'), 'click') 
    const pause$ = fromEvent(document.getElementById('pause'), 'click')
    const reset$ = fromEvent(document.getElementById('reset'), 'click')
    console.log(start$)      

    useEffect(() => {
        const temp = new Date(0);
        temp.setHours(0);
        setTime(temp);


        

    }, []);

    start$.subscribe(()=>{
        timer(1000)
    })




    return(
    <div>    
        <Dial id = 'dial' time = {time}></Dial>
        <button  id= 'start'  size= '14'>start</button>
        <button id='pause'    size= '14'>pause</button>
        <button id='reset'    size= '14'>reset</button>
    </div>
    )

}

export default Stopwatch
